#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <cstdlib>
#include <cooperative_groups.h>
using namespace cooperative_groups;

/* Defines */
#define CUDA_WARP_SIZE (32U)
#define NUM_STRINGS_AFTER_BLAKE2B (1048576U)
#define NUM_BUCKETS (4096U) // Use first 12 bits to of output of blake to determine which bucket the string goes in to
#define NUM_SLOTS_IN_BUCKET (512U) // Number of entries in each bucket - this can be changed but must make sure that threads don't overflow
#define NUM_BITS_TO_SHIFT_TO_ENCODE_BUCKET_ID (64 - 16)
#define NUM_BITS_TO_SHIFT_TO_ENCODE_STRING_ID_A (NUM_BITS_TO_SHIFT_TO_ENCODE_BUCKET_ID - 24)
#define NUM_BITS_TO_SHIFT_TO_ENCODE_STRING_ID_B (NUM_BITS_TO_SHIFT_TO_ENCODE_STRING_ID_A - 24)

typedef struct _kempsieEquihash
{
	uint32_t	roundSlotCounter[NUM_BUCKETS];
	ulong3		roundInputHT[NUM_BUCKETS][NUM_SLOTS_IN_BUCKET];
	ulong3		roundOutputHT[NUM_BUCKETS][NUM_SLOTS_IN_BUCKET];
} kempsieEquihash;

__global__ void shuffle_broadcast_round(kempsieEquihash *input)
{
	uint16_t aui32Collisions[512];
	uint16_t ui32NumCollisions = 0;
	uint16_t aui32SlotPositions[512];
	uint16_t ui32NumSlots = 0;

	const uint8_t ui8MyThreadsCollision = (uint8_t)(input->roundInputHT[blockIdx.x][threadIdx.x].x >> 56);

	/* Process warp */
	for (int32_t i = 0; i < 32; i++)
	{
		/* Broadcast the ith lanes value to all lanes in the warp */
		const uint8_t ui8PeerCollision = __shfl_sync(0xFFFFFFFF, ui8MyThreadsCollision, i); // i is wrong here

		if (ui8MyThreadsCollision == ui8PeerCollision)
		{
			auto g = coalesced_threads();
			uint16_t warp_res;
			if(g.thread_rank() == 0)
			  warp_res = atomicAdd(&input->roundSlotCounter[ui8MyThreadsCollision], g.size());

			/* Send the result to all coalesced threads in the warp */
			g.shfl(warp_res, 0);

			aui32Collisions[ui32NumCollisions++] = i;
			aui32SlotPositions[ui32NumSlots++] = warp_res + g.thread_rank();
		}
	}

	for (int32_t j = 32; j < 512; j+=32)
	{
		const uint8_t ui8MyCollision = (uint8_t)(input->roundInputHT[blockIdx.x][(threadIdx.x + j) % 512].x >> 56);
		/* Process warp */
		for (int32_t i = 0; i < 32; i++)
		{
			/* Broadcast the ith lanes value to all lanes in the warp */
			const uint8_t ui8PeerCollision = __shfl_sync(0xFFFFFFFF, ui8MyCollision, i); // i is wrong here

			if (ui8MyThreadsCollision == ui8PeerCollision)
			{
				auto g = coalesced_threads();
				uint16_t warp_res;
				if(g.thread_rank() == 0)
				  warp_res = atomicAdd(&input->roundSlotCounter[ui8MyThreadsCollision], g.size());

				/* Send the result to all coalesced threads in the warp */
				g.shfl(warp_res, 0);

				aui32Collisions[ui32NumCollisions++] = i;
				aui32SlotPositions[ui32NumSlots++] = warp_res + g.thread_rank();
			}
		}
	}

#pragma unroll
	for (uint32_t i = 0; i < 512; i++)
	{
		const uint32_t slotID = aui32SlotPositions[i];
		if (i < ui32NumCollisions && slotID < NUM_SLOTS_IN_BUCKET)
		{
			unsigned long xorA, xorB, xorC;

			const uint32_t idB = aui32Collisions[i];

			/*
			 * xor 188 bits
			 *
			 * Loading the threadIdx.x part here instead of pre-loading it (as it will always be the same)
			 * is actually faster in wall clock time despite the profiler complaining about
			 * too many accesses
			 *
			 * TODO Investigate this
			 */
			xorA = input->roundInputHT[blockIdx.x][threadIdx.x].x ^ input->roundInputHT[blockIdx.x][idB].x;
			xorB = input->roundInputHT[blockIdx.x][threadIdx.x].y ^ input->roundInputHT[blockIdx.x][idB].y;
			xorC = input->roundInputHT[blockIdx.x][threadIdx.x].z ^ input->roundInputHT[blockIdx.x][idB].z;

			/* Drop the 8 bits used for the collision */
			xorA <<= 8;
			/* Use the next 12 to determine new bucket location */
			const uint32_t newBucketID = (uint32_t)(xorA >> 52);

			//TODO Performance: Seems faster to pre-calculate the xor before the store, rather than doing the calc in the store directly
			/* Store 168 bits */
			xorA = (xorA << 12) | (xorB >> 44);
			xorB = (xorB << (8 + 12)) | (xorC >> 44);
			xorC = xorC  << 20;

			/* Make sure we don't overflow */
			input->roundOutputHT[newBucketID][slotID].x = xorA;
			input->roundOutputHT[newBucketID][slotID].y = xorB;
			input->roundOutputHT[newBucketID][slotID].z = xorC;
		}
	}
}

__global__ void shared_round(kempsieEquihash *input)
{
	__shared__ ulong3 in[512];
	uint32_t aui32Collisions[512];
	uint32_t ui32NumCollisions = 0;

	in[threadIdx.x] = input->roundInputHT[blockIdx.x][threadIdx.x];

	__syncthreads();

	const uint8_t ui8MyCollision = (uint8_t)(in[threadIdx.x].x >> 56);

	for (int32_t i = threadIdx.x + 1; i < 512; i++)
	{
		const uint8_t ui8PotentialCollision = (uint8_t)(in[i].x >> 56);
		if (ui8MyCollision == ui8PotentialCollision)
		{
			/* We have found a collision */

			/* Store out the location(s) in the bucket where the collision was found */
			aui32Collisions[ui32NumCollisions++] = i;
		}
	}


	for (uint32_t i = 0; i < ui32NumCollisions; i++)
	{
		unsigned long xorA, xorB, xorC;

		const uint32_t idB = aui32Collisions[i];

		/*
		 * xor 188 bits
		 *
		 * Loading the threadIdx.x part here instead of pre-loading it (as it will always be the same)
		 * is actually faster in wall clock time despite the profiler complaining about
		 * too many accesses
		 *
		 * TODO Investigate this
		 */
		xorA = in[threadIdx.x].x ^ in[idB].x;
		xorB = in[threadIdx.x].y ^ in[idB].y;
		xorC = in[threadIdx.x].z ^ in[idB].z;

		/* Drop the 8 bits used for the collision */
		xorA <<= 8;
		/* Use the next 12 to determine new bucket location */
		const uint32_t newBucketID = (uint32_t)(xorA >> 52);

		/* Get the slot id */
		const uint32_t ui32SlotID = atomicAdd(&input->roundSlotCounter[newBucketID], 1);

		//TODO Performance: Seems faster to pre-calculate the xor before the store, rather than doing the calc in the store directly
		/* Store 168 bits */
		xorA = (xorA << 12) | (xorB >> 44);
		xorB = (xorB << (8 + 12)) | (xorC >> 44);
		xorC = xorC  << 20;

		/* Make sure we don't overflow */
		if (ui32SlotID < NUM_SLOTS_IN_BUCKET)
		{
			input->roundOutputHT[newBucketID][ui32SlotID].x = xorA;
			input->roundOutputHT[newBucketID][ui32SlotID].y = xorB;
			input->roundOutputHT[newBucketID][ui32SlotID].z = xorC;
		}
	}
}

void byte_perm()
{
	kempsieEquihash *input;
	uint32_t counter[4096];
	ulong3 cpuInput[512];

	cudaMalloc((void**) &input, sizeof(kempsieEquihash));
	if (cudaPeekAtLastError() != cudaSuccess)
		printf("%s\n", cudaGetErrorString(cudaPeekAtLastError()));

	for (int32_t i = 0; i < 4096; i++)
	{
		for (int32_t j = 0; j < 512; j++)
		{
			cpuInput[j].x = rand();
			cpuInput[j].y = rand();
			cpuInput[j].z = rand();
		}

		cudaMemcpy(&input->roundInputHT[i][0], cpuInput, sizeof(cpuInput), cudaMemcpyHostToDevice);
		if (cudaPeekAtLastError() != cudaSuccess)
			printf("%s\n", cudaGetErrorString(cudaPeekAtLastError()));
	}

	cudaMemset(input->roundSlotCounter, 0, 512);
	if (cudaPeekAtLastError() != cudaSuccess)
		printf("%s\n", cudaGetErrorString(cudaPeekAtLastError()));

	shared_round<<<4096, 512>>>(input);

	if (cudaPeekAtLastError() != cudaSuccess)
		printf("%s\n", cudaGetErrorString(cudaPeekAtLastError()));

	cudaMemcpy(counter, input->roundSlotCounter,
			sizeof(input->roundSlotCounter), cudaMemcpyDeviceToHost);
	if (cudaPeekAtLastError() != cudaSuccess)
		printf("%s\n", cudaGetErrorString(cudaPeekAtLastError()));

	uint32_t average = 0;
	for (int32_t i = 0; i < 4096; i++)
	{
		average += counter[i];
	}
	printf("AVERAGE %d\n", average / 4096);



	cudaMemset(input->roundSlotCounter, 0, 512);
	if (cudaPeekAtLastError() != cudaSuccess)
		printf("%s\n", cudaGetErrorString(cudaPeekAtLastError()));

	shuffle_broadcast_round<<<4096, 512>>>(input);

	if (cudaPeekAtLastError() != cudaSuccess)
		printf("%s\n", cudaGetErrorString(cudaPeekAtLastError()));

	cudaMemcpy(counter, input->roundSlotCounter,
			sizeof(input->roundSlotCounter), cudaMemcpyDeviceToHost);
	if (cudaPeekAtLastError() != cudaSuccess)
		printf("%s\n", cudaGetErrorString(cudaPeekAtLastError()));

	average = 0;
	for (int32_t i = 0; i < 4096; i++)
	{
		average += counter[i];
	}
	printf("AVERAGE %d\n", average / 4096);


	cudaFree(input);
	if (cudaPeekAtLastError() != cudaSuccess)
		printf("%s\n", cudaGetErrorString(cudaPeekAtLastError()));
}

int main(int argc, char *argv[])
{
	byte_perm();

	return 0;
}
